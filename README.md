# WickedDAG

A project exploring the inferential consequences of conditioning on composite measures. We will study frailty indices used in the medical sciences, which summarise the health status of elderly people.
