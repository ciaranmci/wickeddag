To view and edit a draw.io file, go to https://app.diagrams.net/, where you can upload your XAML file.
A desktop version is also available.

To get image files from the application, export a selection using the File > Export.
